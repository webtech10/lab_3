let range_evaluation = document.getElementById("range-evaluation");
let evaluation = document.getElementById("evaluation");

range_evaluation.onchange = function(){
    evaluation.value = range_evaluation.value;
}
evaluation.onchange = function(){
    let num = Number(evaluation.value);
    if (num > range_evaluation.max){ num = range_evaluation.max };
    if (num < range_evaluation.min){ num = range_evaluation.min };
    evaluation.value = num;
    range_evaluation.value = num;
}